#include<stdio.h>
#include<stdlib.h>

int anio_biciesto(int anio);
int anio(int a);
int mes(int m);
int dia(int d, int m, int a);

int main()
{
    // variables utilizadas
    int yyyy, mm, dd;

    // titulo del programa
    printf("---[ Fechas reales ]---\n");
    // solicitud de anio, mes y dia
    printf("Escribe un anio [formato: yyyy]: ");
    scanf("%d", &yyyy);
    printf("Escribe un mes [formato: mm]: ");
    scanf("%d", &mm);
    printf("Escribe un dia [formato: dd]: ");
    scanf("%d", &dd);

    // funcionalidad del programa
    // validar que anio, mes y dia sean numeros
    if (yyyy > 0 || mm > 0 || dd > 0)
    {
        if (dia(dd,mm,yyyy) == 0 || mes(mm) == 0 || anio(yyyy) == 0)
        {
            printf("La fecha no es una fecha real\n");
        }
        else
        {
        printf("La fecha %d/%d/%d, es una fecha real\n", dia(dd, mm, yyyy), mes(mm), anio(yyyy));
        }
    }
    
    system("pause");
    return 0;
}

// metodo para calcular anio biciesto
int anio_biciesto(int anio)
{
    int a;
    
        if (anio%4 == 0 || anio%100 == 0 || anio%400 == 0)
        {
            a = 1;
        }
        return a;
}

// metodo para obtener anio <>
int anio(int a)
{
    if (a > 1951)
    {
        return a;
    }
    else
    {
        return 0;
    }
}
// metodo para obtener mes
int mes(int m)
{
    if (m < 13 && m > 0)
    {
        return m;
    }
    else
    {
        return 0;
    }
}
// metodo para obtener dia
int dia(int d, int m, int a)
{
    if (anio_biciesto(a) == 1 && d > 0)
    {
        if ((m == 1 || m == 3 || m == 5 || m == 7 || m == 8 || m == 10 || m == 12) && d < 32)
        {
            return d;
        }
        else if ((m == 4 || m == 6 || m == 9 || m == 11 ) && d < 31)
        {
            return d;
        }
        else if (m == 2  && d < 30)
        {
            return d;
        }
        else
        {
            return 0;
        }
    }
    else
    {
        if ((m == 1 || m == 3 || m == 5 || m == 7 || m == 8 || m == 10 || m == 12) && d < 32)
        {
            return d;
        }
        else if ((m == 4 || m == 6 || m == 9 || m == 11 ) && d < 31)
        {
            return d;
        }
        else if (m == 2  && d < 29)
        {
            return d;
        }
        else
        {
            return 0;
        }
    }
}