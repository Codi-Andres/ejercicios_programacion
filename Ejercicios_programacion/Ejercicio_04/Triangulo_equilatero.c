// librerias utilizadas
#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>

// menu principal
int main()
{
    // variables utilizadas
    float lado;
    bool detener_ciclo = false;
    // titulo del programa
    printf("Conocer perimetro de un triangulo equilatero\n");
    // comprueba (itera) que el valor del lado sea correcto
    do
    {
        // solicita el lado del triangulo
        printf("Escribe un lado del triangulo: ");
        
        // verifica que el lado sea numero y positivo
        if ( scanf("%f", &lado) == 0 || lado < 0)
        {
            // mensaje de error
            printf("Error. Ingrese un valor correcto.\n");
            fflush(stdin);
        }
        else
        {
            // muestra y calcula el perimetro
            printf("El perimetro del triangulo equilatero es: %.2f\n", lado * 3);
            detener_ciclo = true;
        }
    } while (detener_ciclo != true);

    system("pause");
    return 0;
}