// librerias utilizadas
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main()
{
    // variables utilizadas
    int anio, valor;
    char cadena[4];
    // titlo del programa
    printf("---[ Calculadora de anio biciesto ]---\n");
    // solicitud del anio
    printf("Escribe un anio[formato: yyyy]: ");
    scanf("%d", &anio);


    cadena[4] = anio;
    valor = strlen(cadena);
    // validar que el anio tenga formato correcto
    if (anio < 1 || valor > 4 )
    {
        printf("Error. Ingrese un valor correcto.");
    }
    else
    {
        // calcular anio biciesto
        if (anio%4 == 0 || anio%100 == 0 || anio%400 == 0)
        {
            printf("El anio %d, es un anio biciesto\n", anio);
        }
        else
        {
            printf("El anio %d, NO es un anio biciesto\n", anio);
        }
    }
    system("pause");
    return 0;
}
