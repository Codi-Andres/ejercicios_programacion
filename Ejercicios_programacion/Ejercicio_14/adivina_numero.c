#include<stdio.h>
#include<stdlib.h>
#include<stdbool.h>
#include <time.h>  

int aleatorio();
bool adivina(int n, int r);

int main()
{
    // varibles utilizadas
    int numero, num_azar;
    bool gano;
    // titulo del programa
    printf("---[ Adivina el numero ]---\n");
    // instrucciones del juego
    printf("___ Reglas: Encuentra el numero del 1 al 100 para ganar. ____\n");

    // solicitud del numero
    printf("Escribe el numero: ");
    scanf("%d", &numero);
    // comprobar que numero sea positivo y numero
    if (numero < 1 || numero > 100)
    {
        printf("Error. Ingrese un valor correcto.\n");
    }
    else
    {
        // genera un numero al azar
        num_azar = aleatorio();

        // funcionalidad principal del programa
        // jugbilidad
        gano = adivina(numero, num_azar);
        if (gano == true)
        {
            printf("Ganaste!!!!\nEncontraste el numero: %d\n", num_azar);
        }
        else
        {
            printf("Perdiste!!!!\nNO encontraste el numero: %d\n", num_azar);
        }
    }
    system("pause");
    return 0;
}

// metodo que genera un numero aleatorio
int aleatorio()
{
    srand(time(NULL));
    return 1 + rand() % (101 - 1);
}

// metodo con la jugabilidad para adivinar el numero
bool adivina(int n, int r)
{
    int contador = 1;
    bool gano;
    do
    {
        if(n == r)
        {
        contador = 5;
        gano = true;
        }
        else
        {
            printf("Intenta otra vez. Escribe un numero: ");
        scanf("%d", &n);
            contador ++;
            gano = false;
        }
    } while (contador != 5);
    return gano;
}
