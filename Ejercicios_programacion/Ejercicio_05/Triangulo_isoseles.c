// librerias utilizadas
#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>

// menu principal
int main()
{
    // variables utilizadas
    float lados, base;
    bool detener_ciclo = false;
    // titulo del programa
    printf("Perimetro de un triangulo isoseles\n");
    // comprueba (itera) que el valor de los lados y base sea correcto
    do
    {
        // solicita los lados del triangulo
        printf("Escribe el valor de los lados iguales del triangulo: ");
        // verifica que los lados sean numero y positivo
        if (scanf("%f", &lados) == 0 || lados < 0)
        {
            // mensaje de error
            printf("Error. Ingrese un valor correcto para los lados del triangulo.\n");
            fflush(stdin);
        }
        else
        {
            // solicita la base del triangulo
            printf("Escribe el valor de la base del triangulo: ");
            // verifica que la base sea numero, positivo e igual a los lados
            if (scanf("%f", &base) == 0 || base < 0 || base == lados)
            {
                // mensaje de error
                printf("Error. Ingrese un valor correcto para la base del triangulo.\n");
                fflush(stdin);
            }
            else
            {
                // muestra y calcula el perimetro
                printf("El perimetro del triangulo isoseles es: %.2f\n", (2*lados) + base);
                detener_ciclo = true;
            }
        }
    } while (detener_ciclo != true);

    system("pause");
    return 0;
}