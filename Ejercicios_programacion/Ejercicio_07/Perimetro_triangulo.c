#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

// variables globales utilizadas
int opc;
float lado_derecho, lado_izquierdo, base, perimetro;
bool validar = false;

float equilatero(float lado_derecho);
bool validacion_NumPositivo(float l);
float isoseles(float lado_derecho, float base);
float escaleno(float base, float lado_izquierdo, float lado_derecho);

// metodo principal main
int main()
{

    // funcionalidad del programa
    do
    {
        // titulo del programa
        printf("____PERIMETRO DE UN TRIANGULO____\n");
        // menu del programa
        printf("---[  Menu principal  ]---\n");
        printf("1. Equilatero\n");
        printf("2. Isoseles\n");
        printf("3. Escaleno\n");
        printf("4. Salir\n");
        printf("Escribe una opcion: ");
        scanf("%d", &opc);

        // opciones del menu
        switch (opc)
        {
        case 1:
            // solicita el lado del triangulo
            printf("Escribe un lado del triangulo: ");
            scanf("%f", &lado_derecho);
            // calcula perimetro
            perimetro = equilatero(lado_derecho);
            printf("El perimetro es: %.2f\n", perimetro);
            system("pause");
            break;
        case 2:
            // solicita los lados del triangulo
            printf("Escribe el valor de los lados iguales del triangulo: ");
            scanf("%f", &lado_derecho);
            // solicita la base del triangulo
            printf("Escribe el valor de la base del triangulo: ");
            scanf("%f", &base);
            // calcula perimetro
            perimetro = isoseles(lado_derecho, base);
            printf("El perimetro es: %.2f\n", perimetro);
            system("pause");
            break;
        case 3:
            // calcula perimetro
            perimetro = escaleno(base, lado_izquierdo, lado_derecho);
            printf("El perimetro es: %.2f\n", perimetro);
            system("pause");
            break;
        case 4:
            system("pause");
            break;

        default:
            break;
        }

    } while (opc != 4);

    return 0;
}

// metodo para calcular perimetro de triangulo equilatero
float equilatero(float lado_derecho)
{
    validar = validacion_NumPositivo(lado_derecho);
    //  comprobar que el valor ingresado sea correcto
    if (validar == true)
    {
        // retorna el perimetro
        return lado_derecho * 3;
    }
}

// metodo para calcular perimetro de triangulo isoseles
float isoseles(float lado_derecho, float base)
{
    // comprobar que los valores ingresados sean correctos
    if (validacion_NumPositivo(lado_derecho) == true && validacion_NumPositivo(base) == true)
    {
        // verifica que la base no sea igual a los lados
        if (base == lado_derecho)
        {
            // mensaje de error
            printf("Error. Ingrese un valor correcto para la base o lados del triangulo.\n");
            fflush(stdin);
        }
        else
        {
            // retorna el perimetro
            return (2 * lado_derecho) + base;
        }
    }
}

// metodo para calcular perimetro de triangulo escaleno
float escaleno(float base, float lado_izquierdo, float lado_derecho)
{
    // ingreso de la base, lado izquierdo y derecho del triangulo
    printf("Escribe el valor de la base del triangulo: ");
    scanf("%f", &base);
    printf("Escribe el valor del lado izquierdo del triangulo: ");
    scanf("%f", &lado_izquierdo);
    printf("Escribe el valor del lado derechos del triangulo: ");
    scanf("%f", &lado_derecho);

    // comprobar que los valores ingresados sean correctos
    if (validacion_NumPositivo(lado_derecho) == true && validacion_NumPositivo(base) == true && validacion_NumPositivo(lado_izquierdo) == true)
    {
        // verifica que la base, lado izquierdo y derecho sean diferentes
        if (base == lado_derecho || base == lado_izquierdo || lado_derecho == lado_izquierdo)
        {
            // mensaje de error
            printf("Error. Existen lados iguales en el triangulo.\n");
            fflush(stdin);
            
        }
        else
        {
            // retorna el perimetro
            return base + lado_izquierdo + lado_derecho;
        }
    }
}

// metodo que verifica que el valor sea un numero y positivo
bool validacion_NumPositivo(float l)
{
    if (l == 0 || l < 0)
    {
        // mensaje de error
        printf("Error. Ingrese un valor correcto para los lados del triangulo.\n");
        fflush(stdin);
        return false;
    }
    else
    {
        return true;
    }
}