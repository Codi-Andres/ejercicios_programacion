#include<stdio.h>
#include<stdlib.h>

int main()
{
    // variables utilizadas
    int n;
    // titulo de programa
    printf("---[ tabla de multiplicar ]---\n");
    // solicitud de numero
    printf("Escribe un numero: ");
    scanf("%d", &n);
    system("cls");
    // funcionalidad del programa
    for (int i = 2; i < 11; i++)
    {
        printf(" %d x %d = %d\n", n, i, n*i);
    }
    system("pause");
    return 0;
}
