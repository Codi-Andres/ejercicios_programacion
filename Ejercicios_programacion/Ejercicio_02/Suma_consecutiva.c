// librerias utilizadas
#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>

// menu principal
int main()
{
    // variables utilizadas
    int numero, sum;
    bool numero_correcto;

    // titulo del programa
    printf("Ingrese un numero del 0 al 50");
    // comprueba (itera) que el valor de numero sea correcto
    do
    {
        // solicita un numero
        printf("\nEscribe un numero: ");
        //scanf("%d", &numero);
        // valida que numero se encuentre entre 1 y 50
        if (scanf("%d", &numero) == 0 || numero < 1 || numero > 50)
        {
            // muestra error y no aapermite terminar el bucle while
            printf("\nError. Ingrese un numero del 0 al 50.");
            numero_correcto = false;
            fflush(stdin); 
        }
        else
        {
            // suma los numeros consecutivos desde 1 hasta numero
            for(int i = 0; i < numero + 1; i++)
            {
                sum += i;
            }
            // detiene ciclo while
            numero_correcto = true;
        }
    }while(numero_correcto != true);

    // muestra el resultado de la suma
    printf("La suma del 1 al %d es de: %d\n", numero, sum);
    system("pause");

    return 0;
}