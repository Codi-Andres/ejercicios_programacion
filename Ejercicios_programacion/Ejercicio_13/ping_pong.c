#include<stdio.h>
#include<stdlib.h>

int main()
{
    // imprimir numeros del 1 al 100
    for (int i = 1; i < 101; i++)
    {
        if (i%3 == 0 && i%5 == 0)
        {
            printf("ping-pong\n");
        }
        else if(i%3 == 0)
        {
            printf("ping\n");
        }
        else if (i%5 == 0)
        {
            printf("pong\n");
        }
        else
        {
            printf("%d\n", i);
        }
    }
    system("pause");
    return 0;
}
