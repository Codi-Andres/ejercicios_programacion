#include <stdio.h>
#include <stdlib.h>

int main()
{
    // variables utilizadas
    int lado1, lado2, lado3;
    // titulo del programa
    printf("---[ Desigualdad de un triangulo ]---\n");
    // solicitud de los lados del triangulo
    printf("Escribe el primer lado: ");
    scanf("%d", &lado1);
    printf("Escribe el segundo lado: ");
    scanf("%d", &lado2);
    printf("Escribe el tercer lado: ");
    scanf("%d", &lado3);
    // funcionalidad del programa
    // verificar desigualdad
    if (lado1 < 1 || lado2 < 1 || lado3 < 1)
    {
        printf("Error. Ingrese un valor correcto.");
    }
    else
    {
        if ((lado1 + lado2) > lado3 ||
            (lado2 + lado3) > lado1 ||
            (lado1 + lado3) > lado2)
        {
            printf("Estas longitudes SI forman un triangulo\n");
        }
        else
        {
            printf("Estas longitudes NO forman un triangulo\n");
        }
    }
    system("pause");
    return 0;
}
