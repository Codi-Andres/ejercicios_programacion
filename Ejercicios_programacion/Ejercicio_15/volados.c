#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <stdbool.h>

int volado();
int input_volado();
int apuesta();

int main()
{
    // variables utilizadas
    int tu_dinero, pc_dinero, turno=0, apuesta_jugador, apuesta_pc, cara_moneda;
    // titulo del programa
    printf("---[ Juego de volados ]---\n");

    // jugador
    tu_dinero = 500;

    // jugador PC
    pc_dinero = 500;

    // funcionalidad del juego
    do
    {
        // muestra dtos del jugador
        printf(" [ Tu turno ]\n");
        printf("Tu dinero: %d\n", tu_dinero);
        // ingreso de apuesta del jugador
        apuesta_jugador = apuesta();
        // ingreso de eleccion del volado
        cara_moneda = input_volado();

        // muestra dtos del jugador pc
        printf("\n [ Turno del PC ]\n");
        printf("Dinero del contrincante: %d\n", pc_dinero);
        // ingreso de apuesta de pc
        srand(time(NULL));
        apuesta_pc = 20 + rand() % (101 - 20);
        printf("El contrincante aposto: %d\n", apuesta_pc);

        // inicio del volado
        printf("\nSe lanzo la moneda...\n");
        system("pause");
        if (cara_moneda == 0)
        {
            turno = 3;
        }
        else if (cara_moneda == volado())
        {
            tu_dinero += apuesta_pc;
            pc_dinero -= apuesta_pc;
            turno++;
            printf("Acertaste.\n\n");
        }
        else
        {
            pc_dinero += apuesta_jugador;
            tu_dinero -= apuesta_jugador;
            turno++;
            printf("Tu contrincante acerto.\n\n");
        }

    } while (turno != 3);

    // obtencion del ganador
    if (tu_dinero > pc_dinero)
    {
        printf("Ganaste!!!\nTe llevas: %d\n", tu_dinero);
    }
    else
    {
        printf("Perdiste!!!\nTe llevas: %d\n", tu_dinero);
    }

    system("pause");
    return 0;
}

// metodo que calcula el volado
int volado()
{
    srand(time(NULL));
    return 1 + rand() % (3 - 1);
}

// metodo que muestra sol o aguila
int input_volado()
{
    int opcion, n;
    printf("--  Elige  --\n");
    printf("| 1. Sol    |\n");
    printf("| 2. Aguila |\n");
    printf("-------------\n");
    printf("Escribe una opcion: ");
    scanf("%d", &opcion);
    switch (opcion)
    {
    case 1:
        n = 1;
        break;
    case 2:
        n = 2;
        break;
    default:
        printf("Error. Ingrese una opcion correcta.\n");
        n = 0;
        break;
    }
    return n;
}

// metodo que obtiene la apuesta del jugador
int apuesta()
{
    int a;
    bool parar;
    do
    {
        parar = false;
        printf("Escribe tu apuesta [Min 20 - Max 100]: ");
        scanf("%d", &a);
        if (a > 19 && a < 101)
        {
            parar = true;
        }
    } while (parar != true);
    return a;
}