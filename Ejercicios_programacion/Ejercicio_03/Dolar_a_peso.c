// librerias utilizadas
#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>

// menu principal
int main()
{
    // variables utilizadas
    float dolar, peso;
    bool numero_correcto;

    // titulo del programa 
    printf("Conversion de Dolares a Pesos[MX]");
    do
    {
        // solicita una cantidad
        printf("\nEscribe una cantidad (Dolares): ");
        
        // validar que la cantidad sea numerica y positiva
        if (scanf("%f", &dolar) == 0 || dolar < 0)
        {
            // mensaje de error
            printf("\nError. Ingrese solo numeros.");
            numero_correcto = false;
            fflush(stdin);
        }
        else
        {
            // convercion de dolar a peso
            peso = dolar * 20.0181; 
            numero_correcto = true;
        }
    } while (numero_correcto != true);
    // muestra el resultado
    printf("\nLa cantidad de $%.2f dolares, equivale a $%.2f pesos mexicanos\n", dolar, peso);
    system("pause");

    return 0;
}