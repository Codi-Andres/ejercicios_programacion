#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main()
{
    // variables utilizadas
    char* mes;
    int m;
    // titulo del programa
    printf("---[ Mes del anio ]---\n");
    // solicitud del mes
    printf("Escribe el mes: ");
    scanf("%d", &mes);
    m =  (int)mes;
    // obteniendo el mes
    switch (m)
    {
    case 0:
        printf("Enero\n");
        break;
    case 1:
        printf("Febrero\n");
        break;
    case 2:
        printf("Marzo\n");
        break;
    case 3:
        printf("Abril\n");
        break;
    case 4:
        printf("Mayo\n");
        break;
    case 5:
        printf("Junio\n");
        break;
    case 6:
        printf("Julio\n");
        break;
    case 7:
        printf("Agosto\n");
        break;
    case 8:
        printf("Septiembre\n");
        break;
    case 9:
        printf("Octubre\n");
        break;
    case 10:
        printf("Noviembre\n");
        break;
    case 11:
        printf("Diciembre\n");
        break;
    default:
        printf("No es un mes\n");
        break;
    }
    system("pause");
    return 0;
}
