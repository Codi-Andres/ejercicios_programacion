// librerias utilizadas
#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>

// menu principal
int main()
{
    // variables utilizadas
    float lado_derecho, lado_izquierdo, base;
    bool detener_ciclo = false;
    // titulo del programa
    printf("Perimetro de un triangulo escaleno\n");

    // comprueba (itera) que el valor de cada uno de los lados sea correcto
    do
    {
        // solicita la base del triangulo
        printf("Escribe el valor de la base del triangulo: ");
        // verifica que los lados sean numero y positivo
        if (scanf("%f", &base) == 0 || base < 0)
        {
            // mensaje de error
            printf("Error. Ingrese un valor correcto para los lados del triangulo.\n");
            fflush(stdin);
        }
        else
        {
            // solicita el lado izquierdo del triangulo
            printf("Escribe el valor del lado izquierdo del triangulo: ");
            // verifica que la base sea numero, positivo y diferente a la base
            if (scanf("%f", &lado_izquierdo) == 0 || lado_izquierdo < 0 || base == lado_izquierdo)
            {
                // mensaje de error
                printf("Error. Ingrese un valor correcto para el lado izquierdo del triangulo.\n");
                fflush(stdin);
            }
            else
            {
                // solicita el lado derecho del triangulo
                printf("Escribe el valor del lado derecho del triangulo: ");
                // verifica que la base sea numero, positivo y diferente a la base y lado izquierdo
                if (scanf("%f", &lado_derecho) == 0 || lado_derecho < 0 || base == lado_derecho || lado_derecho == lado_izquierdo)
                {
                    // mensaje de error
                    printf("Error. Ingrese un valor correcto para el lado derecho del triangulo.\n");
                    fflush(stdin);
                }
                else
                {
                    // muestra y calcula el perimetro
                    printf("El perimetro del triangulo escaleno es: %.2f\n", (lado_derecho + lado_izquierdo) + base);
                    detener_ciclo = true;
                }
            }
        }
    } while (detener_ciclo != true);

    system("pause");
    return 0;
}